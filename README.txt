
This sitecrawler is built on the Scrapy framework.
It is thoroughly documented here:
<link> http://doc.scrapy.org/en/latest/index.html

=======================================================
INSTALLING ON THE WINDOWS MACHINES IN THE ANIMATION PIT
=======================================================

Mostly follow these directions, mostly:
<link> http://doc.scrapy.org/en/latest/intro/install.html

Python 2.7.10 and pip should already be on the machines
though you may need to add C:\Python\Scripts to the “Path” system variable

Get the correct version of pywin per the instructions:
<example filename> pywin32-219.win-amd64-py2.7.exe

Pip may run into errors with the Visual C++ Compiler:
get that sorted out by downloading:
<link> http://www.microsoft.com/en-us/download/details.aspx?id=44266
<filename> VCForPython27.msi

Pip may then also run into errors with lxml:
get that sorted out by downloading:
<link> http://www.lfd.uci.edu/~gohlke/pythonlibs/
<filename> lxml-3.4.4-cp27-none-win_amd64.whl
pip install that wheel file


===================
RUNNING THE CRAWLER
===================

In the command line, navigate to the folder directory that contains the scrapy.cfg file.
Run: $ scrapy crawl [spider name]
You can abort an in progress crawl via CTRL + C

Available spiders by name:
------------------
truCat
This spiders is primed to begin on the ToysRUs all categories page
and drill down every category until it finds products.
Runs for about 10 hours.

truReckless
This spider starts at www.toysrus.com and crawls EVERY link it sees
on every link it goes to.
I have not idea how long this will take.

truSiteMap
This spider uses ToysRUs’s own sitemap as a reference for all the links
to check for products.
Runs for about 8 hours.

truDeBug
This spider is the bottom-most function and tests what information
is crawled on a single product page.
When using it, you must pass a start_url argument in the command line.
For example: $ scrapy crawl truDeBug -a start_url=“http://www.toysrus.com/product/index.jsp?productId=3544816"

truRecklessIndex
This spider is based off the the truReckless spider.
However, it stores the skus it has already crawled in the index.txt file
in this directory level.
Whenever you run this crawler, it will never crawl skus already in index.txt.
The idea is that each successful run will only look for new products
and you can combine all the outputs into a unique list.
The index.txt file itself also contains a growing unique list of possible skus
and one can build a crawler that uses this list to only target product pages.

Optional arguments:
-------------------
$ -o [file]
specifies an output to save the data as (json, csv, check Scrapy documentation for supported types)
example: $ scrap crawl -o crawls/mondaycrawl.csv

$ -s JOBDIR=[path]
specifies a directory to save the crawl state in
if you stop the crawl with CTRL + C, use this argument to start the same crawl from where it stopped
example: $ scrapy crawl tru -s JOBDIR=crawls/tru2








===================================
USING SCRAPYJS TO RENDER JAVASCRIPT
===================================
this only applies to the Branch of the project that implements ScrapyJS
development for this Branch is currently incomplete and not supported

setup:

$ pip install scrapyjs

you will need to download docker toolbox

$ docker pull scrapinghub/splash

edit scrapy config file per:
https://github.com/scrapinghub/scrapy-splash

run:

the docker quickstart terminal

$ docker run -p 8050:8050 scrapinghub/splash

$ scrapy crawl ### -o ###.json -s JOBDIR=crawls/###
