# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ToysrusItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    # timestamp when the page was crawled
    time = scrapy.Field()
    # url of the crawled page so that one can visit and check
    url = scrapy.Field()
    # product name
    name = scrapy.Field()
    # manufacturer name
    manufacturer = scrapy.Field()
    # price that it is currently retailing at; could be marked down and thus
    # different than the list price
    retailprice = scrapy.Field()
    # price that the product is listed to be sold as; should always be non-zero
    listprice = scrapy.Field()
    # toysrus product sku value
    sku = scrapy.Field()
    # field that indicates whether a video was found for the product and perhaps
    # also links to it to provide additional information
    video = scrapy.Field()
    pass
