# Scrapy
import scrapy
# for cleaning urls
from urlparse import urlparse, parse_qs
import re
# for logging timestamps
import datetime
# for finding video file urls
import invodo
# for checking video file urls
import httplib
# for capturing the DEFAULT_REQUEST_HEADERS
from toysrus.items import ToysrusItem

# check url function
def checkUrl(url):
    p = urlparse(url)
    conn = httplib.HTTPConnection(p.netloc)
    conn.request('HEAD', p.path)
    resp = conn.getresponse()
    if resp.status < 400:
        return url
    else:
        return "false"

# check for an empty return
def notfound(array):
    if not array:
        return "false"
    else:
        return array[0]



# full Toys R Us spider; run only if you've got lots of time to sink
class TRUspider(scrapy.Spider):
    name = "truCat"
    allowed_domains = ["www.toysrus.com"]
    # these are the Toys R Us "All Categories" pages
    start_urls = [
        "http://www.toysrus.com/category/index.jsp?categoryId=2273442",
        "http://www.toysrus.com/category/index.jsp?categoryId=2273443"
    ]

    # look for all the sub-categories within the div#categoryIndexTRU
    def parse(self, response):
        for href in response.selector.css("#categoryIndexTRU a::attr('href')"):
            # check where it links to next and extract that url
            pagelevel = href.re("^\/(\w+)\/")
            url = response.urljoin(href.extract())
            # if another category send to parse_category
            if pagelevel == [u"category"]:
                yield scrapy.Request(url,callback=self.parse_category)
            # if family level, send to parse_family
            elif pagelevel == [u"family"]:
                yield scrapy.Request(url,callback=self.parse_family)
            # if product level, send to parse_prod
            elif pagelevel == [u"product"]:
                url = response.urljoin(urlparse(url).path + "?productId=" + \
                    parse_qs(urlparse(url).query)['productId'][0])
                yield scrapy.Request(url,callback=self.parse_prod)
        for href in response.selector.css("#categoryIndexTRU a::attr('href')"):
            # check where it links to next and extract that url
            pagelevel = href.re("^\/(\w+)\/")
            url = response.urljoin(href.extract())
            # if another category send to parse_category
            if pagelevel == [u"category"]:
                yield scrapy.Request(url,callback=self.parse_category)
            # if family level, send to parse_family
            elif pagelevel == [u"family"]:
                yield scrapy.Request(url,callback=self.parse_family)
            # if product level, send to parse_prod
            elif pagelevel == [u"product"]:
                url = response.urljoin(urlparse(url).path + "?productId=" + \
                    parse_qs(urlparse(url).query)['productId'][0])
                yield scrapy.Request(url,callback=self.parse_prod)

    # look for subcategoies within the leftnav_container div#module_Taxonomy1
    def parse_category(self, response):
        for href in response.selector.css("#module_Taxonomy1 a::attr('href')"):
            # check where it links to next and extract that url
            pagelevel = href.re("^\/(\w+)\/")
            url = response.urljoin(href.extract())
            # if another category send to parse_category
            if pagelevel == [u"category"]:
                yield scrapy.Request(url,callback=self.parse_category)
            # if family level, send to parse_family
            elif pagelevel == [u"family"]:
                yield scrapy.Request(url,callback=self.parse_family)
            # if product level, send to parse_prod
            elif pagelevel == [u"product"]:
                url = response.urljoin(urlparse(url).path + "?productId=" + \
                    parse_qs(urlparse(url).query)['productId'][0])
                yield scrapy.Request(url,callback=self.parse_prod)

    # on family pages that list products, grab each product within the product
    # loop and send on to parse_prod
    # also look in div#pagination for further family pages and recurse
    def parse_family(self, response):
        for href in response.selector.css("div.prodloop_float > \
            div.prodloop_cont > div.varHeightTop > a::attr('href')"):
            url = response.urljoin(href.extract())
            url = response.urljoin(urlparse(url).path + "?productId=" + \
                parse_qs(urlparse(url).query)['productId'][0])
            yield scrapy.Request(url,callback=self.parse_prod)

        for href in response.css("#pagination a::attr('href')"):
                page_url = response.urljoin(href.extract())
                yield scrapy.Request(page_url,callback=self.parse_family)

    # when a product page is finally reached, extract the desired info
    def parse_prod(self, response):
        item = ToysrusItem()
        item['time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        item['url'] = response.url
        item['name'] = notfound(response.selector.css("#lTitle h1::text")\
            .extract())
        item['manufacturer'] = notfound(response.selector.css('.first label\
            ::text').extract())
        item['retailprice'] = notfound(response.selector.css("#price li.retail \
                span::text").extract())
        item['listprice'] = notfound(response.selector.css("#price li.list span\
            ::text").extract())
        skuvalue = notfound(response.selector.css("input[name='productId']\
            ::attr(value)").extract())
        item['sku'] = skuvalue
        item['video'] = checkUrl(invodo.invodoHasher(skuvalue))
        yield item



# this spider indiscriminately grabs every link on the toysrus site
class TRUFullSpider(scrapy.Spider):
    name = "truReckless"
    allowed_domains = ["www.toysrus.com"]
    start_urls = [
        "http://www.toysrus.com/"
    ]

    def parse(self, response):
        # if we happen to be on a product page, good job, get the info
        if response.selector.css("input[name='productId']::attr(value)")\
            .extract():
            item = ToysrusItem()
            item['time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            item['url'] = response.url
            item['name'] = notfound(response.selector.css("#lTitle h1::text")\
                .extract())
            item['manufacturer'] = notfound(response.selector.css('.first \
                label::text').extract())
            item['retailprice'] = notfound(response.selector.css("#price \
                li.retail span::text").extract())
            item['listprice'] = notfound(response.selector.css("#price li.list \
                span::text").extract())
            skuvalue = notfound(response.selector.css("input[name='productId']\
                ::attr(value)").extract())
            item['sku'] = skuvalue
            item['video'] = checkUrl(invodo.invodoHasher(skuvalue))
            yield item
        # now grab all links on the page
        for href in response.selector.css("a::attr('href')"):
            pagelevel = href.re("\/(\w+)\/")
            url = response.urljoin(href.extract())
            # if we are redirecting to product pages, clean the url
            if pagelevel == [u"product"]:
                url = response.urljoin(urlparse(url).path + "?productId=" + \
                    parse_qs(urlparse(url).query)['productId'][0])
                yield scrapy.Request(url,callback=self.parse)
            elif href.re("(&fg=)"):
                pass
            else:
                yield scrapy.Request(url,callback=self.parse)



# this spider indiscriminately grabs every link on the toysrus site
# however, it also checks against the outputted index of products to never crawl
# an old product again. The idea is that the crawler can be run across multiple
# sessions and continue to add new products to its full catalog in the most
# efficient way possible
class TRUFullSpiderIndexer(scrapy.Spider):
    name = "truRecklessIndex"
    allowed_domains = ["www.toysrus.com"]
    start_urls = [
        "http://www.toysrus.com/"
    ]

    def parse(self, response):
        # if we happen to be on a product page, good job, get the info
        if response.selector.css("input[name='productId']::attr(value)")\
            .extract():
            item = ToysrusItem()
            item['time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            item['url'] = response.url
            item['name'] = notfound(response.selector.css("#lTitle h1::text")\
                .extract())
            item['manufacturer'] = notfound(response.selector.css('.first \
                label::text').extract())
            item['retailprice'] = notfound(response.selector.css("#price \
                li.retail span::text").extract())
            item['listprice'] = notfound(response.selector.css("#price li.list \
                span::text").extract())
            skuvalue = notfound(response.selector.css("input[name='productId']\
                ::attr(value)").extract())
            item['sku'] = skuvalue
            item['video'] = checkUrl(invodo.invodoHasher(skuvalue))

            fa = open("index.txt","a")
            fa.write(skuvalue + "\n")
            fa.close()

            yield item

        fr = open("index.txt")
        readlist = fr.readlines()
        crawledlist = [line.strip() for line in readlist]
        fr.close()

        # now grab all links on the page
        for href in response.selector.css("a::attr('href')"):
            pagelevel = href.re("\/(\w+)\/")
            url = response.urljoin(href.extract())
            # if we are redirecting to product pages, do the following
            if pagelevel == [u"product"]:

                # the following index check should really be done at the
                # middleware level of scrapy; however since I was too lazy to
                # create a separate crawler project folder for this one, and I
                # didn't want the middleware settings to affect the other 4
                # crawlers, this check lives here in the parse loop
                skutocheck = parse_qs(urlparse(url).query)['productId'][0]

                found = False
                for sku in crawledlist:
                    if skutocheck == sku:
                        found = True
                        break
                if(found == True):
                    pass
                else:
                    url = response.urljoin(urlparse(url).path + "?productId=" \
                        + parse_qs(urlparse(url).query)['productId'][0])
                    yield scrapy.Request(url,callback=self.parse)

            elif href.re("(&fg=)"):
                pass
            else:
                yield scrapy.Request(url,callback=self.parse)


# bottom most level spider; test how parse behaves directly on product pages
# use it by passing a start_url argument in the command line
# e.g. scrapy crawl truDeBug -a start_url="http://www.toysrus.com/product/ \
# index.jsp?productId=3544816"
class TRUDebugSpider(scrapy.Spider):
    name = "truDeBug"
    allowed_domains = ["www.toysrus.com"]

    def __init__(self, *args, **kwargs):
        super(TRUDebugSpider, self).__init__(*args, **kwargs)
        self.start_urls = [kwargs.get('start_url')]

    def parse(self, response):
        item = ToysrusItem()
        item['time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        item['url'] = response.url
        item['name'] = notfound(response.selector.css("#lTitle h1::text")\
            .extract())
        item['manufacturer'] = notfound(response.selector.css('.first label\
            ::text').extract())
        item['retailprice'] = notfound(response.selector.css("#price li.retail \
                span::text").extract())
        item['listprice'] = notfound(response.selector.css("#price li.list span\
            ::text").extract())
        skuvalue = notfound(response.selector.css("input[name='productId']\
            ::attr(value)").extract())
        item['sku'] = skuvalue
        item['video'] = checkUrl(invodo.invodoHasher(skuvalue))
        yield item



# sitemap spider instead
from scrapy.spiders import SitemapSpider

class TRUSiteMapSpider(SitemapSpider):
    name = "truSiteMap"
    sitemap_urls = ['https://www.toysrus.com/sitemap.xml']
    sitemap_rules = [
        ('/product/','parse_prod'),
        # ('/buy/','parse_prod')
    ]

    def parse_prod(self, response):
        item = ToysrusItem()
        item['time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        item['url'] = response.url
        item['name'] = notfound(response.selector.css("#lTitle h1::text")\
            .extract())
        item['manufacturer'] = notfound(response.selector.css('.first label\
            ::text').extract())
        item['retailprice'] = notfound(response.selector.css("#price li.retail \
                span::text").extract())
        item['listprice'] = notfound(response.selector.css("#price li.list span\
            ::text").extract())
        skuvalue = notfound(response.selector.css("input[name='productId']\
            ::attr(value)").extract())
        item['sku'] = skuvalue
        item['video'] = checkUrl(invodo.invodoHasher(skuvalue))
        yield item
