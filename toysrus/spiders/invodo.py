def invodoHasher(pod):
    podServer = "//e.invodo.com/4.0/pl/"
    affiliate = "toysrus.com"
    pathHashValue = bdkrHash(affiliate + "/" + pod + ".js")
    url = podServer + str(pathHashValue) + "/" + affiliate + "/" + pod + ".js"
    return url

def bdkrHash(string):
    seed = 131
    hash = 0
    i = 0
    length = len(string)

    for i in range(0,length):
        hash = (((hash * seed) % 0xffff) + ord(string[i])) % 0xffff;

    return abs(hash % 8);

# javascript from invodo.js for reference
#
# invodoHasher = function(pod){
#     var podServer = "//e.invodo.com/4.0/pl/";
#     var affiliate = "toysrus.com";
#     var pathHashValue = bdkrHash(affiliate + "/" + pod + ".js");
#     var url = podServer + pathHashValue + "/" + affiliate + "/" + pod + ".js";
#     console.log(url);
# }
#
# bdkrHash = function(str) {
#     var seed = 131,
#         hash = 0,
#         i = 0,
#         len = str.length;
#     for (i; i < len; i++) {
#         console.log(str.charCodeAt(i))
#         hash = (((hash * seed) % 0xffff) + str.charCodeAt(i)) % 0xffff;
#     }
#     return Math.abs(hash % 8);
# }
