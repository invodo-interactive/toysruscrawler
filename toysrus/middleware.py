#  unused

class CleanUrl(object):
    seen_urls = {}
    def process_request(self, request, spider):
        url = spider.clean_url(request.url)
        if url in self.seen_urls:
              raise IgnoreRequest()
        else:
            self.seen_urls.add(url)
        return request.replace(url=url)
